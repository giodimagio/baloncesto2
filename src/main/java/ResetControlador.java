import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ResetControlador extends HttpServlet {

    private ModeloDatos bd;

    public void init(ServletConfig cfg) throws ServletException {
        bd = new ModeloDatos();
        bd.abrirConexion();
    }

    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        
        HttpSession s = req.getSession(true);
        
        String botonReset = (String) req.getParameter("B3");

        if (botonReset.equalsIgnoreCase("Resetear votaciones")) {
            bd.resetearJugador();
            res.sendRedirect(res.encodeRedirectURL("index.html"));
        }
    }

    public void destroy() {
        bd.cerrarConexion();
        super.destroy();
    }
}
