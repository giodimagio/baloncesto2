# ICDA - Proyecto final 

Máster Universitario en Desarrollo Ágil de Software para la Web (Curso 2020-21)

Asignatura: Integración Continua en el Desarrollo Ágil

Autor: _Jorge Romero Cabrera_

## Sprint
Se ha creado un Sprint en GitLab para el desarrollo de los issues del proyecto
- Board. Inicio
![ICDA_GitLab_Board1](/doc/assets/images/ICDA_GitLab_Board1.png)
- Board. Progreso
![ICDA_GitLab_Board2](/doc/assets/images/ICDA_GitLab_Board2.png)
- Issues
![ICDA_GitLab_Issues1](/doc/assets/images/ICDA_GitLab_Issues1.png)
- Sprint. Inicio
![ICDA_GitLab_Sprint1](/doc/assets/images/ICDA_GitLab_Sprint1.png)
- Sprint. Progreso
![ICDA_GitLab_Sprint2](/doc/assets/images/ICDA_GitLab_Sprint2.png)

## Issues

- **REQ-1:** Como nuevo requisito de la aplicación, la página principal debe ofrecer al usuario un botón “Poner votos a cero”, que al pulsarlo se pongan a 0 los votos de todos los jugadores en la base de datos.
- **REQ-2:** Como nuevo requisito de la aplicación, la página principal debe ofrecer al usuario un botón “Ver votos”, que al pulsarlo muestre una nueva página (por ejemplo, VerVotos.jsp), que muestre una tabla o lista con los nombres de todos los jugadores que hay en la base de datos y sus votos, y un enlace para volver a la página principal.
- **PF-A:** Programar en PruebasPhantomjsIT.java una nueva prueba funcional que simule en la página principal la pulsación del botón “Poner votos a cero”, después la pulsación del botón “Ver votos”, y compruebe que en los votos que aparecen para cada jugador en la página VerVotos.jsp son todos cero.
- **PF-B:** Programar en PruebasPhantomjsIT.java una nueva prueba funcional que introduzca en la caja de la página principal el nombre de un nuevo jugador y marque la opción “Otro”, pulse el botón “Votar”, vuelva a la página principal, simule la pulsación del botón “Ver votos”, y compruebe que en la página VerVotos.jsp ese nuevo jugador tiene 1 voto.
- **PU:** Programar en ModeloDatosTest.java un caso de prueba para el método actualizarJugador(), que compruebe, simulando una base de datos de prueba, que realmente se incrementa en 1 los votos del jugador.
- **QA:** Como garantía de calidad, el código fuente completo del proyecto debe tener un límite de problemas importantes (major issues) de 20 calculados con SonarQube.

## [QA] SonarQube. Garantizar la calidad de código
La resolución de este issue se puede descomponer en tres partes:

 1. Una parte de configuración de un Quality Gate en SonarQube que se active si el número de Major Issues detectados supera los 20.
 2. Otra parte de mejora de calidad, en la cual se da solución a algunos de los 25 Major Issues detectados por SonarQube en el código original, para estar por debajo del límite que hemos establecido en 20 Major issues.
 3. Una tercera parte en la que incluiremos en el `.gitlab-ci.yml` la palabra reservada [allow_failure](https://docs.gitlab.com/ee/ci/yaml/#allow_failure) a la cual asignaremos el valor `false` para que no se siga con la ejecución del pipeline cuando el número de errores supere el quality gate configurado en SonarQube.
 
	 - Parte 1: 25 Major issues del código inicial detectados por SonarQube
	 ![ICDA_Sonarqube_Calidad1](/doc/assets/images/ICDA_Sonarqube_Calidad1.png)

	 - Parte 2: Configuración en SonarQube del quality gate *"Max Number of Major Issues Allowed"*
	 ![ICDA_Sonarqube_MaxIssues1](/doc/assets/images/ICDA_Sonarqube_MaxIssues1.png)
 
	  - Parte 3: Ejecución del pipeline y resultado de las mejoras de calidad en SonarQube
	 ![ICDA_Sonarqube_Calidad2](/doc/assets/images/ICDA_Sonarqube_Calidad2.png)

## [REQ-1] Poner votos a 0
Para resolver este issue se ha añadido la clase `ResetControlador.java` que implementa un Servlet el cual pone los vostos de todos los jugadores de la tabla a 0 llamando al método `resetearJugaro()` del Modelo
 
## Despliegue
La web se ha desplegado en Heroku en Preproducción y Producción:
Pre: https://baloncesto2-pre.herokuapp.com/
Prod: https://baloncesto2.herokuapp.com/